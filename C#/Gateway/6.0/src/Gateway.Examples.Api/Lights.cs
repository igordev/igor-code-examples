﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Lights;
using Igor.Gateway.Dtos.Common;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Lighting;
using Igor.Gateway.Dtos.Lights;

namespace Gateway.Examples.Api
{
    public class Lights
    {
        private readonly ILightService _lightService;

        public Lights()
        {
            _lightService = new LightService("GatewayAppKey", "http://localhost");
        }

        public async Task<LightDto> GetLight()
        {
            var runtimeId = 1;

            var lightDto = await _lightService.Get(runtimeId);

            return lightDto;
        }

        public async Task<IEnumerable<LightDto>> GetAllLights()
        {
            var list = await _lightService.GetAll();

            return list.List;
        }

        public async Task<int> CreateLight()
        {
            var createDto = new CreateLightDto
            {
                Name = "My Light",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                LightType = LightType.Individual,
                MaxLevel = 9000,
                MinLevel = 1000
            };

            var runtimeId = await _lightService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateTunableLight()
        {
            var createDto = new CreateLightDto
            {
                Name = "My Tunable Light",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                LightType = LightType.Tunable,
                MaxLevel = 9000,
                MinLevel = 1000,
                MinimumCCT = 3000,
                MaximumCCT = 6000
            };

            var runtimeId = await _lightService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateLight()
        {
            var runtimeId = 1;

            var updateDto = new UpdateLightDto
            {
                Name = "My Updated Light",
                MaxLevel = 10000,
                MinLevel = 0
            };

            await _lightService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetLightEvents()
        {
            var runtimeId = 1;

            var events = await _lightService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task TurnLightOn()
        {
            var runtimeId = 1;

            await _lightService.TurnOn(runtimeId);
        }

        public async Task TurnLightOff()
        {
            var runtimeId = 1;

            await _lightService.TurnOff(runtimeId);
        }

        public async Task SetLighting()
        {
            var runtimeId = 1;

            var lightingDto = new LightingDto
            {
                Behavior = Behavior.ConstantRate,
                CCT = 5400,
                CurveType = CurveType.Dali,
                Duration = 3000,
                Level = 9000,
                State = OnOffState.On
            };

            await _lightService.SetLighting(runtimeId, lightingDto);
        }

        public async Task UpdateTunableLight()
        {
            var runtimeId = 1;

            var updateDto = new UpdateLightDto
            {
                Name = "My Updated Tunable Light",
                MaxLevel = 10000,
                MinLevel = 0,
                MinimumCCT = 2700,
                MaximumCCT = 6500
            };

            await _lightService.Update(runtimeId, updateDto);
        }

        public async Task DeleteLight()
        {
            var runtimeId = 1;

            await _lightService.Delete(runtimeId);
        }
    }
}