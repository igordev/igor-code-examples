﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.MotionSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.MotionSensors;

namespace Gateway.Examples.Api
{
    public class MotionSensors
    {
        private readonly IMotionSensorService _motionSensorService;

        public MotionSensors()
        {
            _motionSensorService = new MotionSensorService("GatewayAppKey", "http://localhost");
        }

        public async Task<MotionSensorDto> GetMotionSensor()
        {
            var runtimeId = 1;

            var motionSensorDto = await _motionSensorService.Get(runtimeId);

            return motionSensorDto;
        }

        public async Task<IEnumerable<MotionSensorDto>> GetAllMotionSensors()
        {
            var list = await _motionSensorService.GetAll();

            return list.List;
        }

        public async Task<int> CreateMotionSensor()
        {
            var createDto = new CreateMotionSensorDto
            {
                Name = "My Motion Sensor",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom"
            };

            var runtimeId = await _motionSensorService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateMotionSensor()
        {
            var runtimeId = 1;

            var updateDto = new UpdateMotionSensorDto
            {
                Name = "My Updated Motion Sensor"
            };

            await _motionSensorService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetMotionSensorEvents()
        {
            var runtimeId = 1;

            var events = await _motionSensorService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateMotionSensorEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateMotionSensorEventDto
            {
                State = MotionSensorState.Occupancy
            };

            await _motionSensorService.CreateEvent(runtimeId, eventDto);
        }

        public async Task DeleteMotionSensor()
        {
            var runtimeId = 1;

            await _motionSensorService.Delete(runtimeId);
        }
    }
}