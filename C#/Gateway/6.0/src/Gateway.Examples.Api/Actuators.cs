﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Actuators;
using Igor.Gateway.Dtos.Actuators;
using Igor.Gateway.Dtos.Events;

namespace Gateway.Examples.Api
{
    public class Actuators
    {
        private readonly IActuatorService _actuatorService;

        public Actuators()
        {
            _actuatorService = new ActuatorService("GatewayAppKey", "http://localhost");
        }

        public async Task<ActuatorDto> GetActuator()
        {
            var runtimeId = 1;

            var actuatorDto = await _actuatorService.Get(runtimeId);

            return actuatorDto;
        }

        public async Task<IEnumerable<ActuatorDto>> GetAllActuators()
        {
            var list = await _actuatorService.GetAll();

            return list.List;
        }

        public async Task<int> CreateActuator()
        {
            var createDto = new CreateActuatorDto
            {
                Name = "My Actuator",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Commands = new List<CommandCollectionDto>
                {
                    new CommandCollectionDto("My Commands")
                    {
                        Commands = new List<CommandTypeDto>
                        {
                            new CommandTypeDto("Range")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("RangeMin", 100),
                                    new CommandParameterDto("RangeMax", 4000)
                                }
                            },
                            new CommandTypeDto("Discrete")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("DiscreteValue", "Up"),
                                    new CommandParameterDto("DiscreteValue", "Down"),
                                    new CommandParameterDto("DiscreteValue", "Stop"),
                                    new CommandParameterDto("DiscreteValue", "Start")
                                }
                            },
                            new CommandTypeDto("Passthrough16"),
                            new CommandTypeDto("Binary"),
                            new CommandTypeDto("Percentage")
                        }
                    }
                }
            };

            var runtimeId = await _actuatorService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateWindowShade()
        {
            var createDto = new CreateActuatorDtoBuilder()
                .WithName("My Window Shade")
                .WithCommandCollection("Shade Position")
                .BuildCollection()
                .BuildActuator();

            createDto.Subtype = ActuatorSubtype.WindowShade;
            createDto.ExternalId = Guid.NewGuid().ToString();
            createDto.Protocol = "Custom";

            var runtimeId = await _actuatorService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateThermostat()
        {
            var createDto = new CreateActuatorDto
            {
                Name = "My Thermostat",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Commands = new List<CommandCollectionDto>
                {
                    new CommandCollectionDto("Setpoint")
                    {
                        Commands = new List<CommandTypeDto>
                        {
                            new CommandTypeDto("Range")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("RangeMin", 15),
                                    new CommandParameterDto("RangeMax", 33)
                                }
                            }
                        }
                    },
                    new CommandCollectionDto("Mode")
                    {
                        Commands = new List<CommandTypeDto>
                        {
                            new CommandTypeDto("Discrete")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("DiscreteValue", "Heating"),
                                    new CommandParameterDto("DiscreteValue", "Cooling"),
                                    new CommandParameterDto("DiscreteValue", "Auto"),
                                    new CommandParameterDto("DiscreteValue", "Off")
                                }
                            }
                        }
                    },
                    new CommandCollectionDto("Fan Speed")
                    {
                        Commands = new List<CommandTypeDto>
                        {
                            new CommandTypeDto("Discrete")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("DiscreteValue", "Low"),
                                    new CommandParameterDto("DiscreteValue", "Medium-Low"),
                                    new CommandParameterDto("DiscreteValue", "Medium-High"),
                                    new CommandParameterDto("DiscreteValue", "High")
                                }
                            }
                        }
                    }
                }
            };

            var runtimeId = await _actuatorService.Create(createDto);

            return runtimeId;
        }

        public async Task CommandActuator()
        {
            var runtimeId = 1;

            var commandDto = new NativeActuatorCommandParametersDto
            {
                CommandType = "Discrete",
                Value = "Up"
            };
            
            await _actuatorService.CommandNative(runtimeId, commandDto);
        }

        public async Task CommandWindowShade()
        {
            var runtimeId = 1;

            var position = 4000;

            await _actuatorService.CommandPercentage(runtimeId, position, "Shade Position");
        }

        public async Task CommandThermostatMode()
        {
            var runtimeId = 1;

            var mode = "Cooling";

            await _actuatorService.CommandDiscrete(runtimeId, mode, "Mode");
        }

        public async Task CommandThermostatSetpoint()
        {
            var runtimeId = 1;

            var setpoint = 23;

            await _actuatorService.CommandRange(runtimeId, setpoint, "Setpoint");
        }

        public async Task<IEnumerable<CommandCollectionDto>> GetAllActuatorCommands()
        {
            var commands = await _actuatorService.GetUniqueCommandCollections();

            return commands.List;
        }

        public async Task<IEnumerable<EventDto>> GetActuatorEvents()
        {
            var runtimeId = 1;

            var events = await _actuatorService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateActuatorEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateActuatorEventDto
            {
                CommandCollection = "My Commands",
                CommandType = "Range",
                Data = "2000"
            };

            await _actuatorService.CreateEvent(runtimeId, eventDto);
        }

        public async Task UpdateActuator()
        {
            var runtimeId = 1;

            var updateDto = new UpdateActuatorDto
            {
                Name = "My New Actuator Name",
                Commands = new List<CommandCollectionDto>
                {
                    new CommandCollectionDto("My Updated Command")
                    {
                        Commands = new List<CommandTypeDto>
                        {
                            new CommandTypeDto("Range")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("RangeMin", 1000),
                                    new CommandParameterDto("RangeMax", 5000)
                                }
                            },
                            new CommandTypeDto("Discrete")
                            {
                                Parameters = new List<CommandParameterDto>
                                {
                                    new CommandParameterDto("DiscreteValue", "Up"),
                                    new CommandParameterDto("DiscreteValue", "Down")
                                }
                            },
                            new CommandTypeDto("Passthrough16"),
                            new CommandTypeDto("Binary"),
                            new CommandTypeDto("Percentage")
                        }
                    }
                }
            };

            await _actuatorService.Update(runtimeId, updateDto);
        }

        public async Task DeleteActuator()
        {
            var runtimeId = 1;

            await _actuatorService.Delete(runtimeId);
        }
    }
}