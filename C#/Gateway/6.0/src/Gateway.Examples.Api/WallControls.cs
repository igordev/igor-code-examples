﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.WallControls;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.WallControls;

namespace Gateway.Examples.Api
{
    public class WallControls
    {
        private readonly IWallControlService _wallControlService;

        public WallControls()
        {
            _wallControlService = new WallControlService("GatewayAppKey", "http://localhost");
        }

        public async Task<WallControlDto> GetWallControl()
        {
            var runtimeId = 1;

            var wallControlDto = await _wallControlService.Get(runtimeId);

            return wallControlDto;
        }

        public async Task<IEnumerable<WallControlDto>> GetAllWallControls()
        {
            var list = await _wallControlService.GetAll();

            return list.List;
        }

        public async Task<IEnumerable<WallControlButtonDto>> GetWallControlButtons()
        {
            var runtimeId = 1;

            var buttons = await _wallControlService.GetWallControlButtons(runtimeId);

            return buttons.List;
        }

        public async Task<int> CreateWallControl()
        {
            var createDto = new CreateWallControlDto
            {
                Name = "My Wall Control",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Buttons = new List<CreateWallControlButtonDto>
                {
                    new CreateWallControlButtonDto { Name = "100%", ControlCommand = 1, DimmingCommand = 10000 },
                    new CreateWallControlButtonDto { Name = "50%", ControlCommand = 1, DimmingCommand = 5000 },
                    new CreateWallControlButtonDto { Name = "Off", ControlCommand = 0, DimmingCommand = 0 }
                }
            };

            var runtimeId = await _wallControlService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateWallControlWithTenButtons()
        {
            var createDto = new CreateWallControlDto
            {
                Name = "My 10-button Wall Control",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Buttons = new List<CreateWallControlButtonDto>
                {
                    new CreateWallControlButtonDto { Name = "100%", ControlCommand = 1, DimmingCommand = 10000 },
                    new CreateWallControlButtonDto { Name = "90%", ControlCommand = 1, DimmingCommand = 9000 },
                    new CreateWallControlButtonDto { Name = "80%", ControlCommand = 1, DimmingCommand = 8000 },
                    new CreateWallControlButtonDto { Name = "70%", ControlCommand = 1, DimmingCommand = 7000 },
                    new CreateWallControlButtonDto { Name = "60%", ControlCommand = 1, DimmingCommand = 6000 },
                    new CreateWallControlButtonDto { Name = "50%", ControlCommand = 1, DimmingCommand = 5000 },
                    new CreateWallControlButtonDto { Name = "40%", ControlCommand = 1, DimmingCommand = 4000 },
                    new CreateWallControlButtonDto { Name = "30%", ControlCommand = 1, DimmingCommand = 3000 },
                    new CreateWallControlButtonDto { Name = "20%", ControlCommand = 1, DimmingCommand = 2000 },
                    new CreateWallControlButtonDto { Name = "10%", ControlCommand = 1, DimmingCommand = 1000 }
                }
            };

            var runtimeId = await _wallControlService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateWallControl()
        {
            var runtimeId = 1;

            var updateDto = new UpdateWallControlDto
            {
                Name = "My Updated Wall Control",
                Buttons = new List<UpdateWallControlButtonDto>
                {
                    new UpdateWallControlButtonDto { Name = "100", ControlCommand = 1, DimmingCommand = 10000},
                    new UpdateWallControlButtonDto { Name = "75", ControlCommand = 1, DimmingCommand = 7500},
                    new UpdateWallControlButtonDto { Name = "50", ControlCommand = 1, DimmingCommand = 5000},
                    new UpdateWallControlButtonDto { Name = "25", ControlCommand = 1, DimmingCommand = 2500}
                }
            };

            await _wallControlService.Update(runtimeId, updateDto);
        }

        public async Task UpdateWallControlButton()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;

            var updateDto = new UpdateWallControlButtonDto
            {
                Name = "On 100",
                ControlCommand = 1,
                DimmingCommand = 10000
            };

            await _wallControlService.UpdateWallControlButton(wallControlRuntimeId, buttonRuntimeId, updateDto);
        }

        public async Task DeleteWallControl()
        {
            var runtimeId = 1;

            await _wallControlService.Delete(runtimeId);
        }

        public async Task<IEnumerable<EventDto>> GetWallControlEvents()
        {
            var runtimeId = 1;

            var list = await _wallControlService.GetEvents(runtimeId);

            return list.List;
        }

        public async Task CreateButtonPressEvent()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;

            var eventDto = new CreateWallControlEventDto
            {
                ButtonId = buttonRuntimeId,
                EventType = WallControlEventType.Press
            };

            await _wallControlService.CreateEvent(wallControlRuntimeId, eventDto);
        }

        public async Task CreateButtonDoublePressEvent()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;

            var eventDto = new CreateWallControlEventDto
            {
                ButtonId = buttonRuntimeId,
                EventType = WallControlEventType.DoublePress
            };

            await _wallControlService.CreateEvent(wallControlRuntimeId, eventDto);
        }

        public async Task CreateButtonHoldEvent()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;

            var holdStartedEventDto = new CreateWallControlEventDto
            {
                ButtonId = buttonRuntimeId,
                EventType = WallControlEventType.HoldStarted
            };

            await _wallControlService.CreateEvent(wallControlRuntimeId, holdStartedEventDto);

            //External code executing while button being held

            var holdEndedEventDto = new CreateWallControlEventDto
            {
                ButtonId = buttonRuntimeId,
                EventType = WallControlEventType.HoldEnded
            };

            await _wallControlService.CreateEvent(wallControlRuntimeId, holdEndedEventDto);
        }

        public async Task AssignGestureToButton()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;

            var gestureDto = new CreateWallControlGestureDto
            {
                Type = WallControlGestureType.DoublePress,
                ActionSetId = 1
            };

            await _wallControlService.CreateWallControlButtonGesture(wallControlRuntimeId, buttonRuntimeId, gestureDto);
        }

        public async Task UpdateButtonGesture()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;
            var gestureId = 200;

            var gestureDto = new UpdateWallControlGestureDto
            {
                Type = WallControlGestureType.DoublePress,
                ActionSetId = 200
            };

            await _wallControlService.UpdateWallControlButtonGesture(wallControlRuntimeId, buttonRuntimeId, gestureId, gestureDto);
        }

        public async Task RemoveGestureFromButton()
        {
            var wallControlRuntimeId = 1;
            var buttonRuntimeId = 100;
            var gestureId = 200;

            await _wallControlService.DeleteWallControlButtonGesture(wallControlRuntimeId, buttonRuntimeId, gestureId);
        }
    }
}