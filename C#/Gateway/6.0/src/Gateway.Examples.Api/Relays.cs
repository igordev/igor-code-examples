﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Relays;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Relays;

namespace Gateway.Examples.Api
{
    public class Relays
    {
        private readonly IRelayService _relayService;

        public Relays()
        {
            _relayService = new RelayService("GatewayAppKey", "http://localhost");
        }

        public async Task<RelayDto> GetRelay()
        {
            var runtimeId = 1;

            var relayDto = await _relayService.Get(runtimeId);

            return relayDto;
        }

        public async Task<IEnumerable<RelayDto>> GetAllRelays()
        {
            var list = await _relayService.GetAll();

            return list.List;
        }

        public async Task<int> CreateRelay()
        {
            var createDto = new CreateRelayDto
            {
                Name = "My Relay",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                IsInverted = false
            };

            var runtimeId = await _relayService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateRelay()
        {
            var runtimeId = 1;

            var updateDto = new UpdateRelayDto
            {
                Name = "My Updated Relay",
                IsInverted = true
            };

            await _relayService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetRelayEvents()
        {
            var runtimeId = 1;

            var events = await _relayService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CloseRelay()
        {
            var runtimeId = 1;

            await _relayService.Close(runtimeId);
        }

        public async Task OpenRelay()
        {
            var runtimeId = 1;

            await _relayService.Open(runtimeId);
        }

        public async Task DeleteRelay()
        {
            var runtimeId = 1;

            await _relayService.Delete(runtimeId);
        }
    }
}