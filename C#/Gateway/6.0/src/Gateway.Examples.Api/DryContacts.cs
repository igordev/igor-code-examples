﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.DryContacts;
using Igor.Gateway.Dtos.DryContacts;
using Igor.Gateway.Dtos.Events;

namespace Gateway.Examples.Api
{
    public class DryContacts
    {
        private readonly IDryContactService _dryContactService;

        public DryContacts()
        {
            _dryContactService = new DryContactService("GatewayAppKey", "http://localhost");
        }

        public async Task<DryContactDto> GetDryContact()
        {
            var runtimeId = 1;

            var dryContactDto = await _dryContactService.Get(runtimeId);

            return dryContactDto;
        }

        public async Task<IEnumerable<DryContactDto>> GetAllDryContacts()
        {
            var list = await _dryContactService.GetAll();

            return list.List;
        }

        public async Task<int> CreateDryContact()
        {
            var createDto = new CreateDryContactDto
            {
                Name = "My Dry Contact",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                DryContactType = DryContactType.DryContact
            };

            var runtimeId = await _dryContactService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateDryContactSubtype(DryContactType subtype)
        {
            var createDto = new CreateDryContactDto
            {
                Name = "My Dry Contact Subtype",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                DryContactType = subtype
            };

            var runtimeId = await _dryContactService.Create(createDto);

            return runtimeId;
        }

        public async Task<IEnumerable<EventDto>> GetDryContactEvents()
        {
            var runtimeId = 1;

            var events = await _dryContactService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateDryContactEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateDryContactEventDto
            {
                EventType = DryContactEventType.Closed
            };

            await _dryContactService.CreateEvent(runtimeId, eventDto);
        }

        public async Task UpdateDryContact()
        {
            var runtimeId = 1;

            var updateDto = new UpdateDryContactDto
            {
                Name = "My Updated Dry Contact"
            };

            await _dryContactService.Update(runtimeId, updateDto);
        }

        public async Task DeleteDryContact()
        {
            var runtimeId = 1;

            await _dryContactService.Delete(runtimeId);
        }
    }
}