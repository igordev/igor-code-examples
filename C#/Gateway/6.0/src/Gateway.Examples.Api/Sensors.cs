﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.Sensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.Sensors;

namespace Gateway.Examples.Api
{
    public class Sensors
    {
        private readonly ISensorService _sensorService;

        public Sensors()
        {
            _sensorService = new SensorService("GatewayAppKey", "http://localhost");
        }

        public async Task<SensorDto> GetSensor()
        {
            var runtimeId = 1;

            var sensorDto = await _sensorService.Get(runtimeId);

            return sensorDto;
        }

        public async Task<IEnumerable<SensorDto>> GetAllSensors()
        {
            var list = await _sensorService.GetAll();

            return list.List;
        }

        public async Task<int> CreateSensor()
        {
            var createDto = new CreateSensorDto
            {
                Name = "My Sensor",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Subtype = SensorSubtype.Sensor,
                UnitOfMeasure = "ppm"
            };

            var runtimeId = await _sensorService.Create(createDto);

            return runtimeId;
        }

        public async Task<int> CreateSensorSubtype()
        {
            var createDto = new CreateSensorDto
            {
                Name = "My Air Temperature Sensor",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                Subtype = SensorSubtype.AirTemperature,
                UnitOfMeasure = "degrees"
            };

            var runtimeId = await _sensorService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateSensor()
        {
            var runtimeId = 1;

            var updateDto = new UpdateSensorDto
            {
                Name = "My Updated Sensor",
                UnitOfMeasure = "ppm2"
            };

            await _sensorService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetSensorEvents()
        {
            var runtimeId = 1;

            var events = await _sensorService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateSensorEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateSensorEventDto
            {
                EventType = "spaceOccupancy",
                DataType = typeof(int).ToString(),
                Data = "5"
            };

            await _sensorService.CreateEvent(runtimeId, eventDto);
        }

        public async Task DeleteSensor()
        {
            var runtimeId = 1;

            await _sensorService.Delete(runtimeId);
        }
    }
}