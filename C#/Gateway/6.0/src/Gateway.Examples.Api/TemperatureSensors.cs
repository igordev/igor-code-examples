﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.TemperatureSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.TemperatureSensors;

namespace Gateway.Examples.Api
{
    public class TemperatureSensors
    {
        private readonly ITemperatureSensorService _temperatureSensorService;

        public TemperatureSensors()
        {
            _temperatureSensorService = new TemperatureSensorService("GatewayAppKey", "http://localhost");
        }

        public async Task<TemperatureSensorDto> GetTemperatureSensor()
        {
            var runtimeId = 1;

            var temperatureSensorDto = await _temperatureSensorService.Get(runtimeId);

            return temperatureSensorDto;
        }

        public async Task<IEnumerable<TemperatureSensorDto>> GetAllTemperatureSensors()
        {
            var list = await _temperatureSensorService.GetAll();

            return list.List;
        }

        public async Task<int> CreateTemperatureSensor()
        {
            var createDto = new CreateTemperatureSensorDto
            {
                Name = "My Temperature Sensor",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom"
            };

            var runtimeId = await _temperatureSensorService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateTemperatureSensor()
        {
            var runtimeId = 1;

            var updateDto = new UpdateTemperatureSensorDto
            {
                Name = "My Updated Temperature Sensor"
            };

            await _temperatureSensorService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetTemperatureSensorEvents()
        {
            var runtimeId = 1;

            var events = await _temperatureSensorService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateTemperatureSensorEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateTemperatureSensorEventDto
            {
                Temperature = 50,
            };

            await _temperatureSensorService.CreateEvent(runtimeId, eventDto);
        }

        public async Task DeleteTemperatureSensor()
        {
            var runtimeId = 1;

            await _temperatureSensorService.Delete(runtimeId);
        }
    }
}