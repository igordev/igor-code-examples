﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Igor.Gateway.Api.Sdk.Apis.LightSensors;
using Igor.Gateway.Dtos.Events;
using Igor.Gateway.Dtos.LightSensors;

namespace Gateway.Examples.Api
{
    public class LightSensors
    {
        private readonly ILightSensorService _lightSensorService;

        public LightSensors()
        {
            _lightSensorService = new LightSensorService("GatewayAppKey", "http://localhost");
        }

        public async Task<LightSensorDto> GetLightSensor()
        {
            var runtimeId = 1;

            var lightSensorDto = await _lightSensorService.Get(runtimeId);

            return lightSensorDto;
        }

        public async Task<IEnumerable<LightSensorDto>> GetAllLightSensors()
        {
            var list = await _lightSensorService.GetAll();

            return list.List;
        }

        public async Task<int> CreateLightSensor()
        {
            var createDto = new CreateLightSensorDto
            {
                Name = "My Light Sensor",
                ExternalId = Guid.NewGuid().ToString(),
                Protocol = "Custom",
                MaxIlluminance = 230,
                MinIlluminance = 10,
                MaxSensorLevel = 400,
                MinSensorLevel = 0
            };

            var runtimeId = await _lightSensorService.Create(createDto);

            return runtimeId;
        }

        public async Task UpdateLightSensor()
        {
            var runtimeId = 1;

            var updateDto = new UpdateLightSensorDto
            {
                Name = "My Updated Light Sensor",
                MaxIlluminance = 330,
                MinIlluminance = 0,
                MaxSensorLevel = 500,
                MinSensorLevel = 0
            };

            await _lightSensorService.Update(runtimeId, updateDto);
        }

        public async Task<IEnumerable<EventDto>> GetLightSensorEvents()
        {
            var runtimeId = 1;

            var events = await _lightSensorService.GetEvents(runtimeId);

            return events.List;
        }

        public async Task CreateLightSensorEvent()
        {
            var runtimeId = 1;

            var eventDto = new CreateLightSensorEventDto
            {
                SensorLevel = 70
            };

            await _lightSensorService.CreateEvent(runtimeId, eventDto);
        }

        public async Task DeleteLightSensor()
        {
            var runtimeId = 1;

            await _lightSensorService.Delete(runtimeId);
        }
    }
}