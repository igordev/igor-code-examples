. T:\sourcecode\igor-code-examples\PowerShell\Gateway\6.0\SendApiRequest.ps1
<#
Sends an API Request to get a list of all Actuators and prints out the 
results to the console.

Note: The default depth for ConvertTo-Json is 2, so we have to increase
    it if we need to serialize nested Json objects
#>

$results = SendApiRequest -endpoint "actuators" -method "GET" -appKey "d4adb4bd450144bfb2ef86d2ba54e82c" -gatewayIpAddress "192.168.19.70" 
     
$resultList = ConvertTo-Json $results -Depth 6

Write-Host $resultList