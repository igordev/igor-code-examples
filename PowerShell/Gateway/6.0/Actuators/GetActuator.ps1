. T:\sourcecode\igor-code-examples\PowerShell\Gateway\6.0\SendApiRequest.ps1
<#
Sends an API Request to get the properties of an Actuator and prints out 
the properties to the console.
#>

$runtimeId = 113

$results = SendApiRequest -Endpoint "actuators/$runtimeId" -Method "GET" -AppKey "d4adb4bd450144bfb2ef86d2ba54e82c" -GatewayIpAddress "192.168.19.70" 

$results.PSObject.Properties | ForEach-Object {
    if ($_.Value -ne $null){
        Write-Host "$($_.Name): $($_.Value)" 
    }
}