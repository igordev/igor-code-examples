<#
Returns a Bearer token that can be used in the Gateway API authentication headers sent with each API request.
Uses an application key to sign a JWT using SHA256 encryption.

@param appKey   An Application Key that has been created (requires connectivity package)
@return         A System.Collections.Hashtable to be used in the HTTP request headers
#>
function GenerateAuthHeaders([string] $appKey){
    $unixEpochStart = New-Object DateTime 1970,1,1,0,0,0,([DateTimeKind]::Utc)
    $now = [int64]((([datetime]::UtcNow) - $unixEpochStart).TotalSeconds)
    $guid = New-Guid
    [hashtable]$header = @{typ = "jwt"; alg = "HS256"} 
    [hashtable]$payload = @{iat = $now; jti = $guid} 

    $headerjson = $header | ConvertTo-Json
    $payloadjson = $payload | ConvertTo-Json 

    $headerjsonbase64 = [Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($headerjson)).Split('=')[0].Replace('+', '-').Replace('/', '_')
    $payloadjsonbase64 = [Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($payloadjson)).Split('=')[0].Replace('+', '-').Replace('/', '_')

    $toBeSigned = $headerjsonbase64 + "." + $payloadjsonbase64

    $SigningAlgorithm = New-Object System.Security.Cryptography.HMACSHA256
    $SigningAlgorithm.Key = [System.Text.Encoding]::ASCII.GetBytes($appKey)   
    $signature = [Convert]::ToBase64String($SigningAlgorithm.ComputeHash([System.Text.Encoding]::ASCII.GetBytes($toBeSigned))).Split('=')[0].Replace('+', '-').Replace('/', '_')

    $token = "$headerjsonbase64.$payloadjsonbase64.$signature"
    $headers = @{Authorization= "Bearer $token"}
    return $headers
}