. T:\sourcecode\igor-code-examples\PowerShell\Gateway\6.0\Authentication\GenerateAuthHeaders.ps1

<#
Returns a PSCustomObject that can be ingested by calling functions to interact with
the Gateway Runtime API

@param gatewayIpAddress The IP Address of the machine running Igor Gateway Software
@param appKey           An Application Key that has been created (requires connectivity package)
@param endpoint         The name of the API endpoint 
@param method           The rest method to execuse (get, put, post, or delete)
@return                 Response from the Gateway API as a PSCustomObject
#>

function SendApiRequest([string] $gatewayIpAddress, [string] $appKey, [string] $endpoint, [string] $method){
    $url = "http://$gatewayIpAddress/api/$endpoint"

    $headers = GenerateAuthHeaders -AppKey $appKey

    try{
        $response = Invoke-RestMethod -Uri $url -Method $method -Headers $headers
        return $response
    } catch {        
        Write-Host "Error:" $_.Exception.Response.StatusCode.value__
        Write-Host "Message:" $_.Exception.Response.ReasonPhrase
    }
}